import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ContactService } from '../contact/contact.service';

@Component({
  selector: 'app-add-contact',
  templateUrl: './add-contact.page.html',
  styleUrls: ['./add-contact.page.scss'],
})
export class AddContactPage implements OnInit {

  contactForm: FormGroup;

  constructor(private fb: FormBuilder, private contactServ: ContactService, private route: Router) { }

  ngOnInit() {
    this.contactForm = this.fb.group({
      lastName: this.fb.control('', Validators.required),
      firstName: this.fb.control('', Validators.required),
      email: this.fb.control('', Validators.required),
      phone: this.fb.control('', Validators.required),
      category: this.fb.control('', Validators.required)
  }) }

  get lastName()
  {
    return this.contactForm.get('lastName')
  };
  get firstName()
  {
    return this.contactForm.get('firstName')
  }
  get email()
  {
    return this.contactForm.get('email')
  }
  get phone()
  {
    return this.contactForm.get('phone')
  }
  get category()
  {
    return this.contactForm.get('category')
  }

  addContact()
  {
    if(this.contactForm.valid)
    {
      this.contactServ.addContact(this.contactForm.value);
      this.route.navigate(['/home']);
    }
      
  }
}
