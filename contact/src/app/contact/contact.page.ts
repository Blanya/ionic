import { Component, OnInit } from '@angular/core';
import { Contact } from './contact.model';
import { ContactService } from './contact.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {
  type: any = "Friend";
  contacts: Contact[];
  contactsFiltered : Contact[];

  constructor(private contactServ: ContactService) { }

  ngOnInit() {
    this.contacts = this.contactServ.getAllContacts();
    this.contactsFiltered = this.contactServ.getContactByCategorie(this.type);
  }

  segmentChanged(e)
  {
    //Attribué la valeur du segment à type
    this.type = e.detail.value;
    this.contactsFiltered = this.contactServ.getContactByCategorie(this.type);
  }

  ionViewWillEnter()
  {
    this.contacts = this.contactServ.getAllContacts();
    this.contactsFiltered = this.contactServ.getContactByCategorie(this.type);
  }

}
