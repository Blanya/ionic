import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContactPage } from './contact.page';

const routes: Routes = [
  {
    path: '',
    component: ContactPage, 
    // children: [
    //   {path: 'new', 
    //   loadChildren: () => import("../add-contact/add-contact.module").then(m => m.AddContactPageModule)
    //   }
    // ]
  },
  {
    path: 'contact/:id',
    loadChildren: () => import('./contact-detail/contact-detail.module').then( m => m.ContactDetailPageModule)
  }
]


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ContactPageRoutingModule {}
