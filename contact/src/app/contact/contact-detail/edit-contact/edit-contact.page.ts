import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Contact } from '../../contact.model';
import { ContactService } from '../../contact.service';

@Component({
  selector: 'app-edit-contact',
  templateUrl: './edit-contact.page.html',
  styleUrls: ['./edit-contact.page.scss'],
})
export class EditContactPage implements OnInit {
  
  id: any;
  contact: Contact;
  contactForm : FormGroup;

  constructor(private ar: ActivatedRoute, 
              private contactServ: ContactService, 
              private fb: FormBuilder,
              private route: Router) { }

  ngOnInit() {
    this.ar.params.subscribe(params => {
      this.id = params.id;
      this.contact = this.contactServ.getContactById(this.id);
      console.log(this.contact)
    });
    this.contactForm = this.fb.group({
      lastName: this.fb.control('', Validators.required),
      firstName: this.fb.control('', Validators.required),
      email: this.fb.control('', Validators.required),
      phone: this.fb.control('', Validators.required),
      category: this.fb.control('', Validators.required)})

    this.contactForm.patchValue({
      firstName: this.contact[0].firstName,
      lastName: this.contact[0].lastName,
      email: this.contact[0].email,
      phone: this.contact[0].phone,
      category: this.contact[0].category
    })
  }

  get lastName()
  {
    return this.contactForm.get('lastName')
  };
  get firstName()
  {
    return this.contactForm.get('firstName')
  }
  get email()
  {
    return this.contactForm.get('email')
  }
  get phone()
  {
    return this.contactForm.get('phone')
  }
  get category()
  {
    return this.contactForm.get('category')
  }

  deleteContact(id: any)
  {
    this.contactServ.deleteContact(Number(id));
    this.route.navigate(['/home'])
  }

  editContact()
  {
    this.contactServ.updateContact(Number(this.id), this.contactForm.value);
    this.route.navigate(['/home']);
  }
}
