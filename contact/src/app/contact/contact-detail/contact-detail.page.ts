import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Contact } from '../contact.model';
import { ContactService } from '../contact.service';

@Component({
  selector: 'app-contact-detail',
  templateUrl: './contact-detail.page.html',
  styleUrls: ['./contact-detail.page.scss'],
})
export class ContactDetailPage implements OnInit {

  contact : Contact;
  id: number;

  constructor(private aR : ActivatedRoute, private contactServ: ContactService) { }

  ngOnInit() {
    this.aR.paramMap.subscribe(paramMap => {
      if(!paramMap.has("id"))
      {
        return;
      }
      const id = paramMap.get('id');
      this.contact = this.contactServ.getContactById(id);
      console.log(this.contact[0])
  })}

}
